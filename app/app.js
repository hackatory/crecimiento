/*!
 *  Copyright (C) 2018  Eloy Espinaco
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

require('date-input-polyfill')
var d3 = require('d3')

var x = d3.scaleLinear()
    .domain([0, 19])
    .range([75, 593])

var y = d3.scaleLinear()
    .domain([0, 24])
    .range([668, 10])

var graph = d3.select('svg')

function plot (point) {
  graph.append('circle')
    .attr('cx', point.x)
    .attr('cy', point.y)
    .attr('r', 5)

  graph.append('line')
    .attr('x1', point.x)
    .attr('x2', point.x)
    .attr('y1', y(-1))
    .attr('y2', y(20))
    .attr('stroke-width', 2)
    .attr('stroke', 'red')

  graph.append('line')
    .attr('x1', x(-1))
    .attr('x2', x(19))
    .attr('y1', point.y)
    .attr('y2', point.y)
    .attr('stroke-width', 2)
    .attr('stroke', 'red')
}

const form = document.getElementById('datos')
const MILLISECONDS_YEAR = 31536000000

form.addEventListener('submit', function (evt) {
  evt.preventDefault()
  const medida1 = document.getElementById('medicion_1_medida').value
  const medida2 = document.getElementById('medicion_2_medida').value
  const fecha1 = document.getElementById('medicion_1_fecha').valueAsDate
  const fecha2 = document.getElementById('medicion_2_fecha').valueAsDate
  const diferencia = medida2 - medida1
  const intervalo = fecha2 - fecha1
  const velocidad = diferencia / intervalo * MILLISECONDS_YEAR
  const nacimiento = document.getElementById('fecha_nacimiento').valueAsDate
  const edad = ((fecha1 - nacimiento) + (fecha2 - nacimiento)) / 2 / MILLISECONDS_YEAR
  plot({x: x(edad), y: y(velocidad)})
})

const selector_sexo = document.getElementById('selector_sexo')
const ref = document.getElementById('reference_graph')

selector_sexo.addEventListener('change', function (evt) {
  if (this.value === 'Masculino') {
    ref.href.baseVal = 'velocidad_altura_nenes.png'
  } else {
    ref.href.baseVal = 'velocidad_altura_nenas.png'
  }
})

console.log('Initialized app')
