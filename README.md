# Crecimiento

Esta es una aplicación para asistir en la evaluación del crecimiento de las
personas hasta los 19 años de edad, basado en los gráficos publicados por la
[Sociedad Argentina de Pediatría][1].

La aplicación funciona completamente en el lado del cliente utilizando HTML5,
javascript y CSS. Para compilarla se utiliza [Brunch](http://brunch.io).

## Como compilar

La única dependencia es [Node.js](http://nodejs.org), los pasos de instalación
dependeran del sistema operativo.

Para instalar las dependencias ejecutar

    npm install

Para habilitar el servidor en modo de desarrollo utilice

    npm start

Y para compilar para produción

    npm run build

## Licencia

Este programa es software libre y puede ser redistribuido y/o modificado bajo
los términos de la GNU Affero General Public License versión 3 o (si se quiere)
bajo una versión posterior.
