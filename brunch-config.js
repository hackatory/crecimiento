// See http://brunch.io for documentation.
exports.config = {
  files: {
    javascripts: {joinTo: 'app.js'},
    stylesheets: {joinTo: 'app.css'}
  },
  modules: {
    autoRequire: {
      'app.js': ['app']
    }
  },
  plugins: {
    autoReload: {
      enabled: {
        js: true,
        css: true,
        assets: true
      }
    }
  }
}

// brunch keeps track of the file so we should backup-copy it
// vim: set backupcopy=yes:
